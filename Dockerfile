FROM tomcat

MAINTAINER Name <aikaterinitsagaraki@gmail.com>

COPY /target/websocket-hello-*SNAPSHOT.war /usr/local/tomcat/webapps/
CMD ["catalina.sh", "run"]

